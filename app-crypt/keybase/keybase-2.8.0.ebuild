# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

EGO_PN="github.com/keybase/client"

inherit golang-vcs-snapshot systemd

DESCRIPTION="Client for Keybase"
HOMEPAGE="https://keybase.io"
SRC_URI="https://${EGO_PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
RESTRICT="mirror"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="pie"

RDEPEND="
	app-crypt/kbfs
	app-crypt/gnupg
"

G="${WORKDIR}/${P}"
S="${G}/src/${EGO_PN}"

QA_PRESTRIPPED="usr/bin/keybase"

src_compile() {
	export GOPATH="${G}:${S}/go/vendor"
	local mygoargs=(
		-v -work -x
		"-buildmode=$(usex pie pie default)"
		-asmflags "-trimpath=${S}"
		-gcflags "-trimpath=${S}"
		-ldflags "-s -w"
		-tags production
	)
	go build "${mygoargs[@]}" ./go/keybase || die
}

src_install() {
	dobin keybase
	dobin packaging/linux/run_keybase
	systemd_douserunit packaging/linux/systemd/keybase.service
}

pkg_postinst() {
	einfo
	elog "Run the service: keybase service"
	elog "Run the client:  keybase login"
	elog "Restart keybase: run_keybase"
	einfo
}
