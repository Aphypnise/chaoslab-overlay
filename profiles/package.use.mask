# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Ian Moone <gentoo@chaoslab.org> (26 Sep 2018)
# Following profiles/base/package.use.mask:
# Requires >=media-video/ffmpeg-4 (bug 654208)
>=www-client/ungoogled-chromium-69 system-ffmpeg

# Ian Moone <gentoo@chaoslab.org> (26 Sep 2018)
# Following profiles/releases/17.0/package.use.mask:
# https://bugs.gentoo.org/661880
>=www-client/ungoogled-chromium-69 system-icu
