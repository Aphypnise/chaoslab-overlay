####################################################################
#
# When you add an entry to the top of this file, add your name, the date, and
# an explanation of why something is getting masked. Please be extremely
# careful not to commit atoms that are not valid, as it can cause large-scale
# breakage, especially if it ends up in the daily snapshot.
#
## Example:
##
## # Dev E. Loper <developer@gentoo.org> (28 Jun 2012)
## # Masking  these versions until we can get the
## # v4l stuff to work properly again
## =media-video/mplayer-0.90_pre5
## =media-video/mplayer-0.90_pre5-r1
#
# - Best last rites (removal) practices -
# Include the following info:
# a) reason for masking
# b) bug # for the removal (and yes you should have one)
# c) date of removal (either the date or "in x days")
#
## Example:
##
## # Dev E. Loper <developer@gentoo.org> (23 May 2015)
## # Masked for removal in 30 days.  Doesn't work
## # with new libfoo. Upstream dead, gtk-1, smells
## # funny. (bug #987654)
## app-misc/some-package

#--- END OF EXAMPLES ---

# Ian Moone <gentoo@chaoslab.org> (15 May 2018)
# Although the ebuild hosted here is based on the one in main tree, I still
# think that it is quite experimental. You should NOT use this, unless you really
# know what you are doing. Please use the Gentoo's official ebuild.
app-emulation/docker

# Ian Moone <gentoo@chaoslab.rog> (6 Nov 2018)
# Although the package works fine for users upgrading from older versions, the
# welcome screen that shows up for first-time users are "broken". Masking until
# I got the time to properly investigate this.
net-p2p/monero-gui
